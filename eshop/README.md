1. 安装rabbitmq：`apt install rabbitmq-server`, 启动：`sudo rabbitmq-server`
2. 启动Celery: `celery -A eshop worker -l info`
3. 启动redis：`redis-server`
4. 付款交易使用：braintree.com
> 在启动应用时，必须现在`settings`文件中配置braintree的id、keys pair；
数据库：`python manage.py makemigrations` -> `python manage.py migrate`
5. 打印PDF使用`WeasyPrint`，依赖（debian base）：`sudo apt-get install build-essential python3-dev python3-pip python3-setuptools python3-wheel python3-cffi libcairo2 libpango-1.0-0 libpangocairo-1.0-0 libgdk-pixbuf2.0-0 libffi-dev shared-mime-info`
6. `WeasyPrint`：使用django渲染模板，并从该模板生成pdf
7. 创建超级用户：`python manage.py createsuperuser --username user_name`

------

`eshop`：

`payment`：处理付款：添加商品到购物车->查看购物车->输入信用卡信息并付款

`cart`：处理车应用

`orders`：处理订单

`shop`：处理商品

`coupons`：处理折扣（包括：折扣系统、推荐系统）

----

+ 推荐系统（shop/recommender.py）：
  1. 对购物车内的商品进行统计，当完成交易时，递增计数购买的商品
  2. 在商品详情中展示与当前商品同时购买的其它商品
  3. 在购物车页面中同样展示与当前购物车内商品同时购买的其他商品
  
  + 推荐数据需要手动输入（目前）：
    1. `python manage.py shell`
    2. `from shop.recommender import Recommender`
    3. `from shop.models import Product`
    4. `black_tea = Product.objects.get(name='Black Tea')`
    5. `green_tea = Product.objects.get(name='Green Tea')`
    6. `r = Recommender()`
    7. `r.product_bought([black_tea])`
    8. `r.product_bought([green_tea])`
    9. `r.product_bought([green_tea, black_tea])`
    10. `r.suggest_products_for([green_tea])`
    11. => [<Product: Black Tea>]

----

Screenshot:
![products list](https://screenshotscdn.firefoxusercontent.com/images/5f68c095-a76c-4de7-a59a-a69282286c4f.png)
![product detail](https://screenshotscdn.firefoxusercontent.com/images/f4e01728-2a28-413c-afb4-f2df3f82c1f2.jpg)
![cart](https://screenshotscdn.firefoxusercontent.com/images/a9ad665a-ba33-4541-b257-d3d198f7a482.png)
![billinfo](https://screenshotscdn.firefoxusercontent.com/images/448c6dea-8aa9-4dca-bfce-4ff00f1ed015.png)
![checkout](https://screenshotscdn.firefoxusercontent.com/images/74d2b1fc-5178-4ce0-9a14-96e6c36388e0.png)