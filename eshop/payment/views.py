'''
1. 使用braintree模块生成客户端token，用于之后初始化客户端的braintree JavaScript
2. 视图渲染模板，模板使用token加载braintree JavaScript SDK
3. 客户输入付款信息并提交表单，生成一个付款的token nonce，将其发送回视图
4. 视图收到token nonce，使用其通过braintree生成一个交易
'''

from io import BytesIO

import braintree
from django.conf import settings
from django.core.mail import EmailMessage
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
import weasyprint

from orders.models import Order


def payment_process(request):
    '''
    交易处理
    '''
    order_id = request.session.get('order_id')
    order = get_object_or_404(Order, id=order_id)
    
    if request.method == 'POST':
        # 获取nonce
        nonce = request.POST.get('payment_method_nonce', None)
        # 创建、提交交易
        result = braintree.Transaction.sale({
            'amount': '{:.2f}'.format(order.get_total_cost()),
            'payment_method_nonce': 'nonce-from-the-client', # 原本为 `nonce`，但因为使用drop-in ui，所以替换成`'nonce-from-the-client'` https://drw.sh/vasopx
            'options': {
                'submit_for_settlement': True # 自动提交交易进行结算
            }
        })
        
        if result.is_success:
            # 修改订单为已付款
            order.paid = True
            # 保存事务id
            order.braintree_id = result.transaction.id
            order.save()
            
            # 将发票以pdf形式发送邮件
            # 创建发票邮件
            subject = 'My Shop - Invoice no. {}'.format(order.id)
            message = 'Please, find attached the invoice for your recent\
                      purchase.'
            email = EmailMessage(subject, message, 'admin-eshop@example.com',
                                [order.email]) # 使用django的EmailMessage生成一个email对象
            # 生成PDF
            html = render_to_string('orders/order/pdf.html', {'order': order})
            out = BytesIO() # 内存字节缓冲区
            stylesheets = [weasyprint.CSS('https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css')]
            weasyprint.HTML(string=html).write_pdf(out, stylesheets=stylesheets)
            # 附上pdf文件
            email.attach('order_{}.pdf'.format(order.id),
                        out.getvalue(),
                        'application/pdf'
            )
            # 发送email
            email.send()
            
            return redirect('payment:done')
        else:
            return redirect('payment:canceled')
            
    else:
        # 生成token
        client_token = braintree.ClientToken.generate()
        return render(
            request,
            'payment/process.html',
            {
                'order': order,
                'client_token': client_token
            }
        )

def payment_done(request):
    '''
    交易成功
    '''
    return render(request, 'payment/done.html')


def payment_canceled(request):
    '''
    取消交易
    '''
    return render(request, 'payment/canceled.html')
