from .cart import Cart

'''
创建上下文处理器，用于将cart存入request上下文中，以便所有模板都可以使用到cart.
将该上下文处理器（以下的cart函数添加到settings文件中TEMPLATES属性的context_processors中）
'''

def cart(request):
    return {'cart': Cart(request)}