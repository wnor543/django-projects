from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.http import require_POST
from shop.models import Product
from shop.recommender import Recommender
from .cart import Cart
from .form import CartAddProductForm
from coupons.forms import CouponApplyForm


@require_POST #只允许POST方法
def cart_add(request, product_id):
    '''
    添加商品到购物车
    '''
    cart = Cart(request)
    product = get_object_or_404(Product, id=product_id)
    form = CartAddProductForm(request.POST)
    
    if form.is_valid():
        cd = form.cleaned_data
        cart.add(
            product=product,
            quantity=cd['quantity'],
            update_quantity=cd['update']
        )
    
    return redirect('cart:cart_detail')


def cart_remove(request, product_id):
    '''
    删除购物车中的商品
    '''
    cart = Cart(request)
    product = get_object_or_404(Product, id=product_id)
    cart.remove(product)
    return redirect('cart:cart_detail')
    
    
def cart_detail(request):
    '''
    展示购物车内商品
    '''
    cart = Cart(request)
    
    if not cart:
        return render(request, 'cart/empty.html')
    
    for item in cart: # 为每个物品设置表单
        item['update_quantity_form'] = CartAddProductForm(
            initial={'quantity': item['quantity'], 'update': True}
        )
    
    coupon_apply_form = CouponApplyForm()
    
    
    # 添加推荐
    r = Recommender()
    cart_products = [item['product'] for item in cart]
    recommended_products = r.suggest_products_for(cart_products, max_results=4)
    
    return render(request, 'cart/detail.html', {
        'cart': cart,
        'coupon_apply_form': coupon_apply_form,
        'recommended_products': recommended_products
    })