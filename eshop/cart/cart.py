from decimal import Decimal
from django.conf import settings
from coupons.models import Coupon
from shop.models import Product


class Cart(object):
    
    def __init__(self, request):
        '''
        初始化购物车会话
        '''
        self.session = request.session
        cart = self.session.get(settings.CART_SESSION_ID)
        
        if not cart:
            cart = self.session[settings.CART_SESSION_ID] = {}
        
        self.cart = cart
        
        # 保存用户提交的折扣码
        self.coupon_id = self.session.get('coupon_id')
    
    def add(self, product, quantity=1, update_quantity=False):
        '''
        添加商品到购物车或更新数量
        '''
        product_id = str(product.id)
        if product_id not in self.cart:
            self.cart[product_id] = {
                'quantity': 0,
                'price': str(product.price)
            }
        if update_quantity:
            self.cart[product_id]['quantity'] = quantity
        else:
            self.cart[product_id]['quantity'] += quantity
        
        self.save()
    
    def save(self):
        '''
        标记会话为以修改，以确保Django保存该会话
        '''
        self.session.modified = True
    
    def remove(self, product):
        '''
        从购物车删除商品
        '''
        product_id = str(product.id)
        if product_id in self.cart:
            del self.cart[product_id]
            self.save()
    
    def __iter__(self):
        '''
        迭代购物车中的条目以获取数据库中对应商品的实例
        '''
        product_ids = self.cart.keys()
        products = Product.objects.filter(id__in=product_ids)
        
        cart = self.cart.copy()
        for product in products:
            cart[str(product.id)]['product'] = product
        
        for item in cart.values():
            item['price'] = Decimal(item['price'])
            item['total_price'] = item['price'] * item['quantity']
            yield item
    
    def __len__(self):
        '''
        返回购物车中的商品数
        '''
        return sum(item['quantity'] for item in self.cart.values())
    
    def get_total_price(self):
        '''
        购物车内商品总价
        '''
        return sum(Decimal(item['price']) * item['quantity'] for item in self.cart.values())
    
    def clear(self):
        '''
        从会话中删除购物车
        '''
        del self.session[settings.CART_SESSION_ID]
        self.save()
    
    
    @property
    def coupon(self):
        '''
        定义coupon方法作为类的属性，当Cart实例包含coupon_id时，返回Coupon对象
        '''
        if self.coupon_id:
            return Coupon.objects.get(id=self.coupon_id)
        return None
    
    def get_discount(self):
        '''
        获取折扣价
        '''
        if self.coupon:
            return (self.coupon.discount / Decimal('100')) \
                    * self.get_total_price()
        return Decimal('0')
        
    def get_total_price_after_discount(self):
        '''
        减去折扣价后的总价
        '''
        return self.get_total_price() - self.get_discount()
