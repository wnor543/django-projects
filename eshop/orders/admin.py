import csv
import datetime
from django.contrib import admin
from django.http import HttpResponse
from django.urls import reverse
from django.utils.safestring import mark_safe
from .models import Order, OrderItem



'''
自定义admin面板操作，即函数：
1. 显示当前的ModelAdmin
2. 当前的request对象作为HttpRequest实例
3. QuerySet用户选择的对象
'''
def export_to_csv(modeladmin, request, queryset):
    '''
    导出为CSV文件
    '''
    opts = modeladmin.model._meta
    resp = HttpResponse(content_type='text/csv')
    # Content-Disposition头信息表明HTTP响应包含附件
    resp['Content-Disposition'] = 'attachment;'\
        'filename={}.csv'.format(opts.verbose_name)
    writer = csv.writer(resp)
    
    # 通过获取模型的_meta调用方法get_fields获取模型的所有字段
    fields = [field for field in opts.get_fields() if not field.many_to_many\
             and not field.one_to_many]
    
    # csv文件头信息
    writer.writerow([field.verbose_name for field in fields])
    # csv内容
    for obj in queryset:
        data_row = []
        for field in fields:
            value = getattr(obj, field.name)
            if isinstance(value, datetime.datetime):
                value = value.strftime('%Y/%m/%d')
            data_row.append(value)
        writer.writerow(data_row)
    return resp

export_to_csv.short_description = 'Export to CSV' # 在模板中显示的文字


def order_detail(obj):
    '''
    为每个Order添加跳转到order详情的连接,在OrderAdmin中启用
    '''
    return mark_safe('<a href="{}">View</a>'.format(
        reverse('orders:admin_order_detail', args=[obj.id])
    ))


def order_pdf(obj):
    '''
    在admin面板中显示导出pdf操作
    '''
    return mark_safe('<a href="{}">PDF</a>'.format(
        reverse('orders:admin_order_pdf', args=[obj.id])
    ))
order_pdf.short_description = 'Invoice'


'''
集成orders模型到admin面板中
'''
class OrderItemInline(admin.TabularInline):
    model = OrderItem
    raw_id_fields = ['product']


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ['id', 'braintree_id', 'first_name', 'last_name', 'email',
                    'address', 'postal_code', 'city', 'paid',
                    'created', 'updated', order_detail, order_pdf]
    list_filter = ['paid', 'created', 'updated']
    inlines = [OrderItemInline]
    search_fields = ['braintree_id', 'first_name', 'last_name', 'email']
    actions = [export_to_csv] # 在admin面板中激活export_to_csv

