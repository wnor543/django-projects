# Generated by Django 2.0.8 on 2018-08-13 06:52

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='order',
            old_name='fist_name',
            new_name='first_name',
        ),
    ]
