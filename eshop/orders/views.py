from django.conf import settings
from django.contrib.admin.views.decorators import staff_member_required
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from django.urls import reverse
import weasyprint

from cart.cart import Cart
from .models import OrderItem, Order
from .forms import OrderCreateForm
from .tasks import order_created


def order_create(request):
    '''
    创建订单
    '''
    # 从会话中获取购物车信息
    cart = Cart(request)
    if request.method == 'POST':
        form = OrderCreateForm(request.POST)
        if form.is_valid():
            order = form.save(commit=False) # commit=False不存入数据库
            
            if cart.coupon:
                order.coupon = cart.coupon
                order.discount = cart.coupon.discount
            order.save()
            
            for item in cart:
                OrderItem.objects.create(
                    order=order,
                    product=item['product'],
                    price=item['price'],
                    quantity=item['quantity']
                )
            
            # 清空购物车
            cart.clear()
            # 调用异步任务
            order_created.delay(order.id) # delay方法异步执行
            
            # 将定单id保存到会话中，用于之后的payment应用
            request.session['order_id'] = order.id
            return redirect(reverse('payment:process'))
            # return render(request, 'orders/order/created.html', {'order': order})
    
    else:
        form = OrderCreateForm()

    return render(request, 'orders/order/create.html',
                  {'cart': cart, 'form': form})


@staff_member_required
def admin_order_detail(request, order_id):
    '''
    自定义admin面板视图
    '''
    order = get_object_or_404(Order, id=order_id)
    return render(
        request,
        'admin/orders/order/detail.html', #在当前应用的templates目录下，按该解构创建模板
        {
            'order': order,
        }
    )


@staff_member_required
def admin_order_pdf(request, order_id):
    '''
    在admin面板中添加导出pdf操作
    '''
    order = get_object_or_404(Order, id=order_id)
    # 将渲染过后的模板转化为字符串
    html = render_to_string('orders/order/pdf.html', {'order': order})
    
    resp = HttpResponse(content_type='application/pdf')
    resp['Content-Disposition'] = 'filename="order_{}.pdf"'.format(order.id)
    weasyprint.HTML(string=html).write_pdf(
        resp,
        # stylesheets=[weasyprint.CSS(
        # settings.STATIC_ROOT + 'css/pdf.css')]
        stylesheets=[weasyprint.CSS('https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css')],
    )
    
    return resp
