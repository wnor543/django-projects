'''
配合Celery的异步任务模块
'''

from celery import task
from django.core.mail import send_mail
from .models import Order


# 使用装饰器定义celery任务
@task
def order_created(order_id):
    '''
    发送邮件的任务，用以通知用户订单已成功创建
    在需要的视图中调用该任务
    '''
    order = Order.objects.get(id=order_id)
    subject = 'Order nr. {}'.format(order.id)
    message = 'Dear {}, \n\nYou have successfully placed an order.\
                Your order id is {}.'.format(order.first_name, order.id)
    
    mail_sent = send_mail(
        subject,
        message,
        'admin-eshop@example.com',
        [order.email]
    )
    
    return mail_sent
