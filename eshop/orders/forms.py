from django import forms
from .models import Order


class OrderCreateForm(forms.ModelForm):
    '''
    创建订单表单
    '''
    
    email = forms.TextInput(attrs={'class': 'validate'})
    first_name = forms.TextInput(attrs={'class': 'validate'})
    last_name = forms.TextInput(attrs={'class': 'validate'})
    address = forms.TextInput(attrs={'class': 'validate'})
    postal_code = forms.TextInput(attrs={'class': 'validate'})
    city = forms.TextInput(attrs={'class': 'validate'})
    
    class Meta:
        model = Order
        fields = ['first_name', 'last_name', 'email', 'address',
                  'postal_code', 'city']
