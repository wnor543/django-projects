'''
Celery配置文件:将该配置文件导入到django项目的__init__.py文件中，使其随django的
启动而启动
'''

import os
from celery import Celery

# 将Django默认配置加载到Celery中
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'eshop.settings')

# 初始化celery实例
app = Celery('eshop')

# 加载django项目自定义配置文件
app.config_from_object('django.conf:settings', namespace='CELERY')
# 自动查找异步任务
app.autodiscover_tasks()
