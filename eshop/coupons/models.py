from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator


class Coupon(models.Model):
    code = models.CharField(max_length=50, unique=True) # 折扣码
    valid_from = models.DateTimeField() # 折扣码有效期
    valid_to = models.DateTimeField()
    discount = models.IntegerField( # 折扣范围，百分比
                    validators=[MinValueValidator(0), MaxValueValidator(100)]
                )
    active = models.BooleanField() # 折扣码是否有效
    
    def __str__(self):
        return self.code
