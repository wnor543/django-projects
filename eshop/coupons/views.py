from django.shortcuts import render, redirect
from django.utils import timezone
from django.views.decorators.http import require_POST
from .models import Coupon
from .forms import CouponApplyForm


@require_POST
def coupon_apply(request):
    now = timezone.now() # 基于当前用户时区的时间
    form = CouponApplyForm(request.POST)
    msg = ''
    if form.is_valid():
        code = form.cleaned_data['code']
        try: # 验证折扣码是否存在以及有效
            coupon = Coupon.objects.get(
                        code__iexact=code,
                        valid_from__lte=now, # less then or equal
                        valid_to__gte=now,
                        active=True)
            request.session['coupon_id'] = coupon.id
        except Coupon.DoesNotExist:
            request.session['coupon_id'] = None
    return redirect('cart:cart_detail')
