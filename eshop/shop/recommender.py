import redis
from django.conf import settings
from .models import Product


# 连接到redis
r = redis.StrictRedis(
        host=settings.REDIS_HOST,
        port=settings.REDIS_PORT,
        db=settings.REDIS_DB
    )

class Recommender(object):
    
    def get_product_key(self, id):
        # 创建redis键
        return 'product:{}: purchased_with'.format(id)
    
    def product_bought(self, products):
        '''
        获取同时购买的商品
        '''
        product_ids = [p.id for p in products]
        for product_id in product_ids:
            for with_id in product_ids:
                # 获取与每个商品同时购买的其它商品
                if product_id != with_id:
                    # 递增计数同时购买的其它商品
                    r.zincrby(self.get_product_key(product_id),
                                with_id, amount=1)

    def suggest_products_for(self, products, max_results=6):
        '''
        产生推荐的商品
        '''
        product_ids = [p.id for p in products]
        if len(products) == 1:
            # 只有一个商品
            suggestions = r.zrange(self.get_product_key(product_ids[0]),
                                0, -1, desc=True)[:max_results]
        else:
            # 创建一个临时key
            flat_ids = ''.join([str(id) for id in product_ids])
            tmp_key = 'tmp_{}'.format(flat_ids)
            # 有多个产品时，结合所有产品的分数将结果排序集存储在临时key中
            keys = [self.get_product_key(id) for id in product_ids]
            r.zunionstore(tmp_key, keys)
            # 删除推荐所针对的产品的ID
            r.zrem(tmp_key, *product_ids)
            # 降序计数排序商品id
            suggestions = r.zrange(tmp_key, 0, -1, desc=True)[:max_results]
            # 删除临时key
            r.delete(tmp_key)
        suggest_products_ids = [int(id) for id in suggestions]
        
        # 获取推荐商品
        suggested_products = list(Product.objects.filter(id__in=suggest_products_ids))
        suggested_products.sort(key=lambda x: suggest_products_ids.index(x.id))
        
        return suggested_products
    
    def clear_purchases(self):
        '''
        清除产生的推荐商品
        '''
        for id in Product.objects.values_list('id', flat=True):
            r.delete(self.get_product_key(id))
        