from django.contrib import admin
from .models import Category, Product


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug']
    search_fields = ['name', 'slug']
    # prepopulated_fields设置指定字段的值为其它指定字段的值
    prepopulated_fields = {'slug': ('name',)}
    

@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug', 'price', 'availabel',
                    'created', 'updated']
    list_filter = ['availabel', 'created', 'updated']
    list_editable = ['price', 'availabel']
    search_fields = ['name', 'slug']
    prepopulated_fields = {'slug': ('name',)}
